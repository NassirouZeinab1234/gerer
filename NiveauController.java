package controllers;

import javax.inject.Inject;

import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.sql.PreparedStatement;
import play.mvc.Http.Request;
import play.mvc.Result;
import service.Niveau;
import service.NiveauDAO;

public class NiveauController  extends Controller {
	FormFactory dataForm;
	NiveauDAO niveauDAO;
	
	@Inject
	 
	public NiveauController(FormFactory dataForm, NiveauDAO niveauDAO){
		super();
		this.dataForm = dataForm;
		this.niveauDAO = niveauDAO;
		
	}
	public Result ajouterNiveau(Request request ) throws ClassNotFoundException , SQLException{
		 Form<Niveau> form =dataForm.form(Niveau.class).bindFromRequest(request);
		 Niveau niveau =form.get();
		 if(niveauDAO.ajouter(niveau).equals("ok"))
		 System.out.println("ajout effectuer ");
		 else
			 System.out.println("ajout non effectuer :" + niveauDAO.ajouter(niveau));

			
	
	return ok(views.html.ListeNiveau.render(niveauDAO.listes()));
	}
		
	public Result afficherForm(Request request) throws ClassNotFoundException,SQLException {
		return ok(views.html.ajoutNiveau.render(request));
		}
	
	public Result deleteNiveau(Request request, int id_niveau) throws ClassNotFoundException,SQLException{
		niveauDAO. supres(id_niveau);
		return ok(views.html.ListeNiveau.render(niveauDAO.listes()));

	}
}
