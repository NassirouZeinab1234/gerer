
package controllers;

import javax.inject.Inject;

import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.sql.PreparedStatement;
import play.mvc.Http.Request;
import play.mvc.Result;
import service.Versement;
import service.VersementDAO;

public class VersementCtl  extends Controller {
	FormFactory dataForm;
	VersementDAO versementDAO;
	
	@Inject
	 
	public VersementCtl(FormFactory dataForm, VersementDAO versementDAO){
		super();
		this.dataForm = dataForm;
		this.versementDAO = versementDAO;
		
	}
	public Result ajouterVersement(Request request ) throws ClassNotFoundException , SQLException{
		 Form<Versement> form =dataForm.form(Versement.class).bindFromRequest(request);
		 Versement versement =form.get();
		 if(versementDAO.ajouter(versement).equals("ok"))
		 System.out.println("ajout effectuer ");
		 else
			 System.out.println("ajout non effectuer :" + versementDAO.ajouter(versement));

			
	
	return ok(views.html.ListeVersement.render(versementDAO.listes()));
	}
		
	public Result afficherVersement(Request request) throws ClassNotFoundException,SQLException {
		return ok(views.html.ajoutVersement.render(request));
		}
	
	public Result deleteVersement(Request request, int id_versement) throws ClassNotFoundException,SQLException{
		versementDAO. supression(id_versement);
		return ok(views.html.ListeVersement.render(versementDAO.listes()));

	}
	public Result modifViewV(Request request, int id_versement) throws ClassNotFoundException, SQLException{
		return ok(views.html.modifierVersement.render(versementDAO.findByID(id_versement),request));
	}

	public Result updateV(Request request) throws ClassNotFoundException, SQLException {
		  Form<Versement> form = dataForm.form(Versement.class).bindFromRequest(request);
		  Versement e = form.get();
		
		if(versementDAO.update(e).equals("ok")){
			System.out.println("Modification effectuer avec succes");
		}
		else{
			System.err.println("Modification non effectuer" + versementDAO.update(e));
		}
		return ok(views.html.ListeVersement.render(versementDAO.listes()));
		
}
}