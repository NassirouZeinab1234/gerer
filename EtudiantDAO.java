package service;
import java.sql.Date;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;



/**
 * 
 * @author Daouda Djibo
 *
 */

public class EtudiantDAO extends JDBC {
	public String ajouter(Etudiant c) throws ClassNotFoundException,SQLException{
		try{
			preparStat = this.jdbc().prepareStatement("insert into ecole.etudiant values(?,?,?,?,?)");
			preparStat.setString(1, c.getMatricule());
			preparStat.setString(2, c.getNom());
			preparStat.setString(3, c.getPrenom());
			preparStat.setString(4, c.getSexe());
			preparStat.setString(5,  c.getDate_naissance());

			//prepareStat.setDate(4,new java.sql.Date(2009,12,11));
			preparStat.executeUpdate();
			return"ok";


		} catch (SQLException e){
			return e.getMessage();
		}
	}
		public List<Etudiant> listes() throws ClassNotFoundException,SQLException{
			Statment=this.jdbc().createStatement();
			List<Etudiant> listes= new ArrayList<Etudiant>();
			Etudiant c = new Etudiant();
			result = Statment.executeQuery("select * from ecole.etudiant");
			while(result.next()){
				c.setMatricule(result.getString("Matricule"));
				c.setNom(result.getString("Nom"));
				c.setPrenom(result.getString("Prenom"));
				c.setSexe(result.getString("sexe"));
				listes.add(c);
				c = new Etudiant();

			}
			return listes;
	
		}	
		
		public String  supression(String Matricule) throws SQLException, ClassNotFoundException{
			
		String sql="Delete from ecole.etudiant where Matricule=?";
		preparStat = this.jdbc().prepareStatement(sql);
		preparStat.setString(1,Matricule);
		int rowsDeleted= preparStat.executeUpdate();
		if(rowsDeleted>0){
			System.out.println("suppression etudiant succès");
			return "ok";
		}
		else
			return "Erreur";
		}
		
		public Etudiant findByID(String Matricule) throws ClassNotFoundException, SQLException {
			
			Statment = this.jdbc().createStatement();
			Etudiant e = new Etudiant();
				preparStat = this.jdbc().prepareStatement("select * from  ecole.etudiant where Matricule=?");
				preparStat.setString(1, Matricule);
				
				result = preparStat.executeQuery();
				
				if (result.next()){
					e.setMatricule(result.getString("Matricule").replace(" ",""));
					e.setNom(result.getString("nom"));
					e.setPrenom(result.getString("prenom"));
					e.setSexe(result.getString("sexe"));
					e.setDate_naissance(result.getString("date_naissance"));

				}
				return e;
		}
		
		public String update(Etudiant e) throws ClassNotFoundException,SQLException {
			try {
				String sql ="UPDATE ecole.etudiant SET nom=?,prenom=?,sexe=?,date_naissance=? WHERE Matricule=? ";
				preparStat = this.jdbc().prepareStatement(sql);
				preparStat.setString(1, e.getNom());
				preparStat.setString(2, e.getPrenom());
				preparStat.setString(3, e.getSexe());
				preparStat.setString(4, e.getDate_naissance());
				preparStat.setString(5, e.getMatricule());
				
	            int rowsUpdated = preparStat.executeUpdate();
				
				preparStat.executeUpdate();
				
				if (rowsUpdated > 0){
					System.out.println("l'etudiant a ete modifiee avec succes !");
					
				} 
				return"Ok";
				
				
				} catch (Exception ex) {
				return ex.getMessage();
			}
		}
	
		}
				


