package controllers;

import javax.inject.Inject;

import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.sql.PreparedStatement;
import play.mvc.Http.Request;
import play.mvc.Result;
import service.Etudiant;
import service.EtudiantDAO;

public class EtudiantController  extends Controller {
	FormFactory dataForm;
	EtudiantDAO etudiantDAO;
	
	@Inject
	 
	public EtudiantController(FormFactory dataForm, EtudiantDAO etudiantDAO){
		super();
		this.dataForm = dataForm;
		this.etudiantDAO = etudiantDAO;
		
	}
	public Result ajouterEtudiant(Request request ) throws ClassNotFoundException , SQLException{
		 Form<Etudiant> form =dataForm.form(Etudiant.class).bindFromRequest(request);
		 Etudiant etudiant =form.get();
		 if(etudiantDAO.ajouter(etudiant).equals("ok"))
		 System.out.println("ajout effectuer ");
		 else
			 System.out.println("ajout non effectuer :" + etudiantDAO.ajouter(etudiant));

			
	
	return ok(views.html.ListeEtudiant.render(etudiantDAO.listes()));
	}
		
	public Result afficherForm(Request request) throws ClassNotFoundException,SQLException {
		return ok(views.html.ajoutEtudiant.render(request));
		}
	
	public Result deleteEtudiant(Request request, String Matricule) throws ClassNotFoundException,SQLException{
		etudiantDAO. supression(Matricule);
		return ok(views.html.ListeEtudiant.render(etudiantDAO.listes()));

	}
	public Result modifView(Request request, String Matricule) throws ClassNotFoundException, SQLException{
		return ok(views.html.modifierEtudiant.render(etudiantDAO.findByID(Matricule),request));
	}

	public Result updateE(Request request) throws ClassNotFoundException, SQLException {
		  Form<Etudiant> form = dataForm.form(Etudiant.class).bindFromRequest(request);
		  Etudiant e = form.get();
		
		if(etudiantDAO.update(e).equals("ok")){
			System.out.println("Modification effectuer avec succes");
		}
		else{
			System.err.println("Modification non effectuer" + etudiantDAO.update(e));
		}
		return ok(views.html.ListeEtudiant.render(etudiantDAO.listes()));
		
}
}