
package controllers;

import javax.inject.Inject;

import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.sql.PreparedStatement;
import play.mvc.Http.Request;
import play.mvc.Result;
import service.Etudiant;
import service.Matiere;
import service.MatiereDAO;
import service.NiveauDAO;

public class MatiereController  extends Controller {
	FormFactory dataForm;
	MatiereDAO matiereDAO;
	NiveauDAO niveauDAO = new NiveauDAO() ;

	
	@Inject
	 
	public MatiereController(FormFactory dataForm, MatiereDAO matiereDAO){
		super();
		this.dataForm = dataForm;
		this.matiereDAO = matiereDAO;
		
	}
	public Result ajouterMatiere(Request request ) throws ClassNotFoundException , SQLException{
		 Form<Matiere> form =dataForm.form(Matiere.class).bindFromRequest(request);
		 Matiere matiere =form.get();
		 if(matiereDAO.ajouter(matiere).equals("ok"))
		 System.out.println("ajout effectuer ");
		 else
			 System.out.println("ajout non effectuer :" + matiereDAO.ajouter(matiere));

			
	
	return ok(views.html.ListeMatiere.render(matiereDAO.listes()));
	}
		
	public Result afficherForm(Request request) throws ClassNotFoundException,SQLException {
		return ok(views.html.ajoutMatiere.render(request));
		}
	
	public Result deleteMatiere(Request request, int id_matiere) throws ClassNotFoundException,SQLException{
		matiereDAO. supres(id_matiere);
		return ok(views.html.ListeMatiere.render(matiereDAO.listes()));

	}
	public Result modifView(Request request, int id_matiere) throws ClassNotFoundException, SQLException{
		return ok(views.html.modifierMatiere.render(matiereDAO.findByID(id_matiere),request));
	}

	public Result updateE(Request request) throws ClassNotFoundException, SQLException {
		  Form<Matiere> form = dataForm.form(Matiere.class).bindFromRequest(request);
		  Matiere e = form.get();
		
		if(matiereDAO.update(e).equals("ok")){
			System.out.println("Modification effectuer avec succes");
		}
		else{
			System.err.println("Modification non effectuer" + matiereDAO.update(e));
		}
		return ok(views.html.ListeMatiere.render(matiereDAO.listes()));
		
}

}

