package service;
public class Etudiant {
		private String  Matricule;
		private String Nom;
		private String Prenom;
		private String sexe;
		private String date_naissance;
		public String getMatricule() {
			return Matricule;
		}
		public void setMatricule(String matricule) {
			Matricule = matricule;
		}
		public String getNom() {
			return Nom;
		}
		public void setNom(String nom) {
			Nom = nom;
		}
		public String getPrenom() {
			return Prenom;
		}
		public void setPrenom(String prenom) {
			Prenom = prenom;
		}
		public String getSexe() {
			return sexe;
		}
		public void setSexe(String sexe) {
			this.sexe = sexe;
		}
		public String getDate_naissance() {
			return date_naissance;
		}
		public void setDate_naissance(String date_naissance) {
			this.date_naissance = date_naissance;
		}
		
}
